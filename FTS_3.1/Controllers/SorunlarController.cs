﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using FTS_3._1;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace FTS_3._1.Controllers
{
    
    public class SorunlarController : ApiController
    {
        private STS2017Entities db = new STS2017Entities();
        private readonly byte[] TempData;

        // GET: api/Sorunlar
        public IQueryable<TBL_SORUN> GetTBL_SORUN()
        {
            return db.TBL_SORUN;
        }

        // GET: api/Sorunlar/5
        [ResponseType(typeof(TBL_SORUN))]
        public IHttpActionResult GetTBL_SORUN(int id)
        {
            TBL_SORUN tBL_SORUN = db.TBL_SORUN.Find(id);
            if (tBL_SORUN == null)
            {
                return NotFound();
            }

            return Ok(tBL_SORUN);
        }

        // UPLOAD
        [RoutePrefix("api/upload")]
        public class UploadController : ApiController
        {
            private const string Container = "images";

            public static byte[] TempData { get; private set; }

            [HttpPost, Route("")]
            public async Task<IHttpActionResult> UploadFile()
            {
                if (!Request.Content.IsMimeMultipartContent("form-data"))
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                var accountName = ConfigurationManager.AppSettings["storage:account:name"];
                var accountKey = ConfigurationManager.AppSettings["storage:account:key"];
                var storageAccount = new CloudStorageAccount(new StorageCredentials(accountName, accountKey), true);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer imagesContainer = blobClient.GetContainerReference(Container);
                var provider = new AzureStorageMultipartFormDataStreamProvider(imagesContainer);


                try
                {
                    await Request.Content.ReadAsMultipartAsync(provider);
                }
                catch (Exception ex)
                {
                    return BadRequest($"An error has occured. Details: {ex.Message}");
                }

                // Retrieve the filename of the file you have uploaded
                var filename = provider.FileData.FirstOrDefault()?.LocalFileName;
                TempData = filename.Select(Convert.ToByte).ToArray();
                if (string.IsNullOrEmpty(filename))
                {
                    return BadRequest("An error has occured while uploading your file. Please try again.");
                }

                return Ok($"File: {filename} has successfully uploaded");
            }

        }

        // PUT: api/Sorunlar/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTBL_SORUN(int id, TBL_SORUN tBL_SORUN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tBL_SORUN.Id)
            {
                return BadRequest();
            }

            db.Entry(tBL_SORUN).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TBL_SORUNExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sorunlar
        [ResponseType(typeof(TBL_SORUN))]
        public IHttpActionResult PostTBL_SORUN(TBL_SORUN tBL_SORUN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tBL_SORUN.Gorsel = UploadController.TempData;



            db.TBL_SORUN.Add(tBL_SORUN);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tBL_SORUN.Id }, tBL_SORUN);
        }



        // DELETE: api/Sorunlar/5
        [ResponseType(typeof(TBL_SORUN))]
        public IHttpActionResult DeleteTBL_SORUN(int id)
        {
            TBL_SORUN tBL_SORUN = db.TBL_SORUN.Find(id);
            if (tBL_SORUN == null)
            {
                return NotFound();
            }

            db.TBL_SORUN.Remove(tBL_SORUN);
            db.SaveChanges();

            return Ok(tBL_SORUN);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TBL_SORUNExists(int id)
        {
            return db.TBL_SORUN.Count(e => e.Id == id) > 0;
        }
    }
}