﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FTS_3._1;

namespace FTS_3._1.Controllers
{
    public class SorunController : Controller
    {
        private STS2017Entities db = new STS2017Entities();

        // GET: Sorun
        public ActionResult Index()
        {
            return View(db.TBL_SORUN.ToList());
        }

        // GET: Sorun/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_SORUN tBL_SORUN = db.TBL_SORUN.Find(id);
            if (tBL_SORUN == null)
            {
                return HttpNotFound();
            }
            return View(tBL_SORUN);
        }

        // GET: Sorun/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sorun/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Yetkili,Bolge,Aciklama,Tarih,Gorsel,Cozuldu")] TBL_SORUN tBL_SORUN)
        {
            if (ModelState.IsValid)
            { 
                db.TBL_SORUN.Add(tBL_SORUN);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tBL_SORUN);
        }

        // GET: Sorun/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_SORUN tBL_SORUN = db.TBL_SORUN.Find(id);
            if (tBL_SORUN == null)
            {
                return HttpNotFound();
            }
            return View(tBL_SORUN);
        }

        // POST: Sorun/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Yetkili,Bolge,Aciklama,Tarih,Gorsel,Cozuldu")] TBL_SORUN tBL_SORUN)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tBL_SORUN).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tBL_SORUN);
        }

        // GET: Sorun/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_SORUN tBL_SORUN = db.TBL_SORUN.Find(id);
            if (tBL_SORUN == null)
            {
                return HttpNotFound();
            }
            return View(tBL_SORUN);
        }

        // POST: Sorun/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TBL_SORUN tBL_SORUN = db.TBL_SORUN.Find(id);
            db.TBL_SORUN.Remove(tBL_SORUN);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
